// Get the form element
var form  = document.getElementsByTagName("form")[0];
// Get the email input element
var email = document.getElementById("provide_email");

var error = email.nextElementSibling;

// Regular expression to check whether the input is an email address
var emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

// This defines what happens when the user insert the email text
email.addEventListener("input", function () {
  var test = email.value.length > 0 && emailRegExp.test(email.value);

  if (test) {
    email.className = "valid";
    error.innerHTML = "";
  } else {
    email.className = "invalid";
    error.innerHTML = "Please insert an e-mail address";
    error.className = "error";
  }
});


// This defines what happens when the user tries to submit the data
// form.addEventListener("submit", function (event) {
//   var test = email.value.length > 0 && emailRegExp.test(email.value);
//   if (test) {
//     email.className = "valid";
//     error.innerHTML = "";
//     error.className = "error";
//   } else {
//     email.className = "invalid";
//     error.innerHTML = "I expect an e-mail!";
//     error.className = "error active";
//     event.preventDefault();
//   }
// });

form.onsubmit = function () {
   var test = email.value.length > 0 && emailRegExp.test(email.value);

  if (test) {
    email.className = "valid";
    error.innerHTML = "";
    error.className = "error";
  } else {
    email.className = "invalid";
    error.innerHTML = "I expect an e-mail!";
    error.className = "error active";
    return false;
  }
};